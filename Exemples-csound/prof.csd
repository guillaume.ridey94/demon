<CsoundSynthesizer>

<CsOptions>
;-odac         ; real time render
-o prof.wav -W ; file render
</CsOptions>

<CsInstruments>
gi_time_beat  =  60 / 137
instr 1
i_time_release = 0.02
p3 = p3 + i_time_release + 0.01
i_frq = cpspch(4)
i_amp = p5
iffrq  pow 2 ,(p6+3)  
i_frq_filter = i_frq * iffrq
a_decay  expseg 2 ,50 / i_frq_filter ,1 ,1 ,1  
a_release  linseg 1 ,p3 - (i_time_release + 0.01) ,1 ,i_time_release ,0 ,1 ,0  
a_amp =  a_decay * a_release * a_release
k_frq  expseg i_frq*0.5 ,0.01 ,i_frq*0.5 ,0.01 ,i_frq ,1 ,i_frq  
k_frq_filter_attack  port 1 ,0.005 ,0.5  
k_frq_filter_decay  port 0 ,gi_time_beat/2 ,1  
k_frq_filter =  i_frq_filter * k_frq_filter_attack * k_frq_filter_decay
kffrqx  port 0 ,0.05 ,24 * i_amp  
kffrqx =   exp(log(2) * kffrqx / 12)
k_frq_filter_pos_peak =   k_frq_filter / kffrqx
k_frq_filter_neg_peak =   k_frq_filter * kffrqx
a1  phasor k_frq ,0  
a2  phasor k_frq ,0.5  
a1 = 1 - 2 * a1
a2 =   1 - 2 * a2
a1  pareq a1 ,k_frq_filter_pos_peak ,0 ,2.0 ,2  
a2  pareq a2 ,k_frq_filter_neg_peak ,0 ,2.0 ,2  
a0 = a1 - a2
atmp  delay1 a0  
a0 = a0 - atmp
atmp = taninv(a0 * abs(a0) * 400)
a0  integ atmp  
a0  pareq a0 ,k_frq ,0 ,2.0 ,1  
a0 = a0 * 70 * sqrt(sqrt(i_frq_filter))
a0 = a0 * a_amp * i_amp
  out a0  
 endin
instr 2
kSpeed init 1
iSkip init 0
iLoop init 1
a1,asig diskin2 "sample/SynthTom03.wav", kSpeed, iSkip, iLoop
out asig,a1
endin
</CsInstruments>

<CsScore>
t 0    137.00

i1 0.5 0.4 8 0.89 -1.6
i1 1.5 0.4 8 1 -1.3
i1 2.5 0.4 8 0.89 1
i1 3.5 0.4 8 0.78 1.3
</CsScore>

</CsoundSynthesizer>
