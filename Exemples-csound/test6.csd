;jé fé 1 chorus lol

<CsoundSynthesizer>

<CsOptions>
;-odac         ; real time render
-o test6.wav -W ; file render
</CsOptions>

<CsInstruments>
sr = 44100
ksmps = 10
nchnls = 2
0dbfs = 1

gal init 0


instr 1
idur = p3
iamp  =      p4                        ; global amplitude
ifreq =      p5                        ; frequency

asig oscil  iamp, ifreq, 6
klfo lfo (ifreq*1.05946309436-ifreq)*40/100 , 3.7	;j'utilise pas une échelle logarithmique ?	       
asigC oscil iamp,ifreq+klfo, 6
adil delay asigC, 0.035 ;20ms de delay

ares moogvcf2 asig, 2000, .6
aresC moogvcf2 adil, 2000, .6

aenv madsr 0.1, 0.2, 0.7, 0.4
      out    (ares+aresC/2)*aenv, (ares+aresC/2)*aenv                ; output the sound
gal +=(ares+aresC/2)*aenv
endin


	;son sans le chorus
instr 2

idur = p3
iamp  =      p4                        ; global amplitude
ifreq =      p5                        ; frequency

asig oscil  iamp, ifreq, 6

ares moogvcf2 asig, 2000, .6

aenv madsr 0.1, 0.2, 0.7, 0.4
      out    ares*aenv, ares*aenv                ; output the sound
gal +=ares*aenv
endin


	;reverb
instr 99
arev reverb gal, 1
	out arev/8, arev/8
gal = 0
endin

</CsInstruments>

<CsScore>

; square with gen07 and with sinewaves
f 6 0 16384 7  1 8192 1 0 -1 8192 -1   ; square


;iid | sta  | dur  | amp   | freq  
t 0 120

i 2 	0 	1   0.4		220
i 2 	+ 	.   0.4		261.63
i 2	+	2   0.222222	329.63
i 2	^	.   0.222222	392
i 2	^	.   0.222222	493.88
i 1 	5 	1   0.4		220
i 1 	+ 	.   0.4		261.63
i 1	+	2   0.222222	329.63
i 1	^	.   0.222222	392
i 1	^	.   0.222222	493.88
i 99 	0	12


</CsScore>

</CsoundSynthesizer>


