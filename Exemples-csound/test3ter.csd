; même chose que bis, on a passé le son dans un lpf et modulé l'amplitude du deuxième signal

<CsoundSynthesizer>

<CsOptions>
;-odac         ; real time render
-o test3ter.wav -W ; file render
</CsOptions>

<CsInstruments>
sr = 44100
ksmps = 10
nchnls = 2
0dbfs = 1

instr 1 
iamp  =      p4                        ; global amplitude
ifreq =      p5                        ; frequency
klfo lfo 1, 2
ares oscil  iamp, ifreq, 2           ; basic oscillator
asig oscil  iamp, ifreq*2, 6
at moogladder ares + (0.7+0.1*klfo)*asig, 1800, 0.9
aenv madsr 0.3, 0, 1, 0.6
      out    at*aenv, at*aenv                  ; output the sound
endin
</CsInstruments>

<CsScore>
; simple sine wave
f 1 0 4096 10 1 

; triangle with gen07 and with sinewaves
f 2 0 16384 7  0 4096 1 8192 -1 4097 0 ; triangle
f 3 0 4096 10 1 0 1/9 0 1/25 0 1/49 0 1/81		; que les harmoniques impaires l'inverse au carré

; sawtooth with gen07 and with sinewaves
f 4 0 16384 7  -1 16385 1              ; sawtooth
f 5 0 4096 10 1 1/2 1/3 1/4 1/5	1/6 1/7 1/8 1/9	; pour obtenir l'harmonique; juste l'inverse

; square with gen07 and with sinewaves
f 6 0 16384 7  1 8192 1 0 -1 8192 -1   ; square
f 7 0 4096 10 1 0 1/3 0 1/5 0 1/7 0 1/9		;que des harmoniques impairs. On prend l'inverse

f 8 0 4096 10 1 1/4 1/9 1/16 1/25 1/36 1/49 1/64 1/81	; ?

;iid | sta  | dur  | amp   | freq  | ftable
t 0 60

;i 2	0	3 ;tentative de toujours utiliser le même oscilateur plutôt qu'en relancer un à chaque fois
i 1 	0 	1   0.6		220
i 1 	+ 	.   0.6		261.63
i 1	+	2   0.222222	329.63
i 1	^	.   0.222222	392
i 1	^	.   0.222222	493.88


</CsScore>

</CsoundSynthesizer>

