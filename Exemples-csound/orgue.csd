<CsoundSynthesizer>

<CsOptions>
;-odac         ; real time render
-o orgue.wav -W ; file render
</CsOptions>

<CsInstruments>
sr = 44100
ksmps = 10
nchnls = 2
0dbfs = 1

#define shape_oscil1 #2# ; l'utilisateur clojure devra dire quelles formes doivent avoir les oscillateurs et on devra déclarer les ftables de manière appropriée (par exemple, si l'utilisateur ne demande pas de triangle, on le mettra pas dans le fichier
#define shape_oscil2 #7#
#define oscil2_octave #2# ; multiple de 2, l'utilisateur pourrait rentrer directement la puissance de 2 qu'il veut en clojure (par exemple s'il met 3, on met 8 dans csound)
#define balance #0.7# ;par combien on va multiplier l'amplitude du deuxième oscillateur en moyenne compris 0(non compris) et 1 mais n'y est pas limité
#define balance_variation #0.3#
#define balance_variation_frequency #1# ; fréquence en hertz du nombre de changement de variation de la balance
#define low_cut_frequency #1800# ; fréquence du cut-off (Les sons au dessus de cette fréquence seront filtrés)
#define resonance #0.127# ; resonance, generally < 1, but not limited to it. Higher than 1 resonance values might cause aliasing, analogue synths generally allow resonances to be above 1 (source csound manual, moogladder)
#define adsrA #0.3# ; combien de temps dure l'attaque ?
#define adsrD #0# ; combien de temps dure le decay ?
#define adsrS #1# ; hauteur du sustain. Sa longeur vaut 1 - attack - decay - release
#define adsrR #0.6# ; combien de temps dure le release ?

		;nouveau trucs sur l'orgue :
#define balanceOD #0.3# ; permet d'ajuster l'amplitude de chaque canal. En réduisant l'amplitude à droite, on a l'impression que le son vient plus de la gauche par exemple. 
#define balanceOG #1#


instr 1 
iamp  =      p4                        
ifreq =      p5                        
klfo lfo 1, $balance_variation_frequency
ares oscil  iamp, ifreq, $shape_oscil1
asig oscil  iamp, ifreq*$oscil2_octave, $shape_oscil2
at moogladder ares + ($balance+$balance_variation*klfo)*asig, $low_cut_frequency, $resonance
aenv madsr $adsrA, $adsrD, $adsrS, $adsrR ; enveloppe adsr
      out    at*aenv*$balanceOG, at*aenv*$balanceOD                  
endin

		; instrument 2 "synth-lead à ajouter si vous voulez

#define TABLE #4#
#define adsrA2 #0.5# ; combien de temps dure l'attaque ?
#define adsrD2 #0.1# ; combien de temps dure le decay ?
#define adsrS2 #0.6# ; hauteur du sustain. Sa longeur vaut 1 - attack - decay - release
#define adsrR2 #0.2# ; combien de temps dure le release ?
#define balanceLD #1#
#define balanceLG #0.4#
#define ping_frequency #2# ; fréquence du "ping" du synth lead. 2 4 ou 8 c'est pas mal
;but, faire varier entre 1800 et 5000 le kcf. Vu que le lfo a ici une onde carrée on aura toujours 3400-1600 ou 3400+1600 de cutoff frequency (le kcf). Peut-être mieux de demander à l'utilisateur finale les deux fréquences qu'il veut plutôt que lui faire rentrer une valeur moyenne qu'il n'utilisera pas. A moins qu'on propose plus tard d'utiliser d'autres formes d'ondes dans le lfo
#define average_kcf #3400#
#define maxdiff_kcf #1600#

instr 2
idur = p3
iamp  =      p4                        ; global amplitude
ifreq =      p5                        ; frequency

asig oscil  iamp, ifreq, $TABLE           ; basic oscillator



klfo lfo 1, $ping_frequency, 2 ; dernier parmètre, remplacer par un 2 c'est pas mal
ares tone asig, $average_kcf+klfo*1600
aenv madsr $adsrA2, $adsrD2, $adsrS2, $adsrR2

      out    ares*aenv*$balanceLG, ares*aenv*$balanceLD                ; output the sound
endin


</CsInstruments>

<CsScore>
; simple sine wave
f 1 0 4096 10 1 

; triangle with gen07 and with sinewaves
f 2 0 16384 7  0 4096 1 8192 -1 4097 0 ; triangle
f 3 0 4096 10 1 0 1/9 0 1/25 0 1/49 0 1/81		; que les harmoniques impaires l'inverse au carré

; sawtooth with gen07 and with sinewaves
f 4 0 16384 7  -1 16385 1              ; sawtooth
f 5 0 4096 10 1 1/2 1/3 1/4 1/5	1/6 1/7 1/8 1/9	; pour obtenir l'harmonique; juste l'inverse

; square with gen07 and with sinewaves
f 6 0 16384 7  1 8192 1 0 -1 8192 -1   ; square
f 7 0 4096 10 1 0 1/3 0 1/5 0 1/7 0 1/9		;que des harmoniques impairs. On prend l'inverse

f 8 0 4096 10 1 1/4 1/9 1/16 1/25 1/36 1/49 1/64 1/81	; ?

;iid | sta  | dur  | amp   | freq  | ftable
t 0 60
; l'espace correspond à un chagement de mesure. Ne sert qu'à moi pour traduire la partition en csound, ne sert potentiellement à rien pour l'utilisateur final
;Re m
i 1 	0 	2   0.222222	146,83 
i 1	^	.   0.222222	174.61
i 1	^	.   0.222222	220
;Do
i 1 	+ 	2   0.222222	130.81 
i 1	^	.   0.222222	164.81
i 1	^	.   0.222222	196
;Sol
i 1 	+ 	2   0.222222	98 
i 1	^	.   0.222222	123.47
i 1	^	.   0.222222	146.83
;La m
i 1 	+ 	2   0.222222	110 
i 1	^	.   0.222222	130.81
i 1	^	.   0.222222	164.81
;Re m
i 1 	+ 	2   0.222222	146,83 
i 1	^	.   0.222222	174.61
i 1	^	.   0.222222	220
;Do
i 1 	+ 	2   0.222222	130.81 
i 1	^	.   0.222222	164.81
i 1	^	.   0.222222	196
;Sol
i 1 	+ 	2   0.222222	98 
i 1	^	.   0.222222	123.47
i 1	^	.   0.222222	146.83
;La m
i 1 	+ 	2   0.222222	110 
i 1	^	.   0.222222	130.81
i 1	^	.   0.222222	164.81
;Re m
i 1 	+ 	2   0.222222	146,83 
i 1	^	.   0.222222	174.61
i 1	^	.   0.222222	220
;Do
i 1 	+ 	2   0.222222	130.81 
i 1	^	.   0.222222	164.81
i 1	^	.   0.222222	196
;Sol
i 1 	+ 	2   0.222222	98 
i 1	^	.   0.222222	123.47
i 1	^	.   0.222222	146.83
;La m
i 1 	+ 	2   0.222222	110 
i 1	^	.   0.222222	130.81
i 1	^	.   0.222222	164.81
;Re m | Re m
i 1 	+ 	1   0.222222	146,83 
i 1	^	.   0.222222	174.61
i 1	^	.   0.222222	220
i 1 	+ 	1   0.222222	146,83 
i 1	^	.   0.222222	174.61
i 1	^	.   0.222222	220
;Do | Do
i 1 	+ 	1   0.222222	130.81 
i 1	^	.   0.222222	164.81
i 1	^	.   0.222222	196
i 1 	+ 	1   0.222222	130.81 
i 1	^	.   0.222222	164.81
i 1	^	.   0.222222	196
;Sol | Sol
i 1 	+ 	1   0.222222	98 
i 1	^	.   0.222222	123.47
i 1	^	.   0.222222	146.83
i 1 	+ 	1   0.222222	98 
i 1	^	.   0.222222	123.47
i 1	^	.   0.222222	146.83
;La m | La m
i 1 	+ 	1   0.222222	110 
i 1	^	.   0.222222	130.81
i 1	^	.   0.222222	164.81
i 1 	+ 	1   0.222222	110 
i 1	^	.   0.222222	130.81
i 1	^	.   0.222222	164.81
;Mi m
i 1 	+ 	2   0.222222	164.81
i 1	^	.   0.222222	196
i 1	^	.   0.222222	246.94
;Re m
i 1 	+ 	2   0.222222	146,83 
i 1	^	.   0.222222	174.61
i 1	^	.   0.222222	220
;La m
i 1 	+ 	2   0.222222	110 
i 1	^	.   0.222222	130.81
i 1	^	.   0.222222	164.81
;Si
i 1 	+ 	2   0.222222	116.54 
i 1	^	.   0.222222	146.83
i 1	^	.   0.222222	174.61
;Mi m
i 1 	+ 	2   0.222222	164.81
i 1	^	.   0.222222	196
i 1	^	.   0.222222	246.94
;Re m
i 1 	+ 	2   0.222222	146,83 
i 1	^	.   0.222222	174.61
i 1	^	.   0.222222	220
;La m
i 1 	+ 	2   0.222222	110 
i 1	^	.   0.222222	130.81
i 1	^	.   0.222222	164.81
;Sol
i 1 	+ 	2   0.222222	98 
i 1	^	.   0.222222	123.47
i 1	^	.   0.222222	146.83
;Re m
i 1 	+ 	2   0.222222	146,83 
i 1	^	.   0.222222	174.61
i 1	^	.   0.222222	220
;Do
i 1 	+ 	2   0.222222	130.81 
i 1	^	.   0.222222	164.81
i 1	^	.   0.222222	196
;Sol
i 1 	+ 	2   0.222222	98 
i 1	^	.   0.222222	123.47
i 1	^	.   0.222222	146.83
;La m
i 1 	+ 	2   0.222222	110 
i 1	^	.   0.222222	130.81
i 1	^	.   0.222222	164.81
;Re m
i 1 	+ 	2   0.222222	146,83 
i 1	^	.   0.222222	174.61
i 1	^	.   0.222222	220
;Do
i 1 	+ 	2   0.222222	130.81 
i 1	^	.   0.222222	164.81
i 1	^	.   0.222222	196
;Sol
i 1 	+ 	2   0.222222	98 
i 1	^	.   0.222222	123.47
i 1	^	.   0.222222	146.83
;La m
i 1 	+ 	2   0.222222	110 
i 1	^	.   0.222222	130.81
i 1	^	.   0.222222	164.81
;Re m
i 1 	+ 	4   0.222222	146,83 
i 1	^	.   0.222222	174.61
i 1	^	.   0.222222	220

;synth-lead
i 2	8.5	0.25	0.5	440
i 2	+	0.25	0.5	523.25
i 2	+	1	0.5	587.33

i 2	^+1.75	0.25	0.5	392 ;on a ici un silence entre cette note et la précédente, il faut calculer à partir du début de la note précédente car on ne peut pas combiner + et ^+. on a donc la durée du silence+la longueur de la note précédente.
i 2	+	0.25	0.5	587.33
i 2	+	0.75	0.5	523.25

i 2	^+1.75	0.25	0.33333	349,23 
i 2	^	.	0.33333	523,25 
i 2	+	0.75	0.33333	392 
i 2	^	.	0.33333	587,33

i 2	^+1.25	0.25	0.5	440
i 2	+	0.25	0.5	587.33
i 2	+	0.25	0.5	659.26
i 2	+	0.25	0.5	698.46
i 2	+	0.25	0.5	587.33
i 2	+	0.25	0.5	880

i 2	+	0.25	0.5	440
i 2	+	0.25	0.5	293.66
i 2	+	0.25	0.5	440
i 2	+	0.25	0.5	293.66
i 2	+	0.25	0.5	587.33
i 2	+	0.25	0.5	293.66
i 2	+	0.25	0.5	587.33
i 2	+	0.25	0.5	293.66

i 2	+	0.25	0.5	523.25
i 2	+	0.25	0.5	392
i 2	+	0.25	0.5	523.25
i 2	+	0.25	0.5	392
i 2	+	0.25	0.5	659.26
i 2	+	0.25	0.5	261.63
i 2	+	0.25	0.5	659.26
i 2	+	0.25	0.5	261.63

;on entend pas l'attaque, il faudrait un autre instrument
i 2	^+1	0.25	0.5	392
i 2	+	0.16666666666	0.5	392
i 2	+	0.16666666666	0.5	392
i 2	+	0.16666666666	0.5	392
i 2	+	0.16666666666	0.5	392
i 2	+	0.16666666666	0.5	392
i 2	+	0.16666666666	0.5	392

i 2	22.25	0.25	0.5	440 ;Ici je n'indique pas le début de la note par rapport à la note précédente mais par rapport au début du score. La raison étant qu'utiliser des tierces me semble ajouter un décalage.
i 2	^+0.5	0.25	0.5	523.25
i 2	^+0.5	0.25	0.5	659.26
i 2	^+0.5	0.25	0.5	880


i 2	^+0.5	0.25	0.5	698,46
i 2	+	0.25	0.5	587.33
i 2	+	0.25	0.5	440
i 2	^+0.5	0.25	0.5	587.33
i 2	+	0.25	0.5	440
i 2	+	0.25	0.5	293.66

i 2	^+0.5	0.25	0.5	659.26
i 2	+	0.25	0.5	523.25
i 2	+	0.25	0.5	392
i 2	^+0.5	0.25	0.5	659.26
i 2	+	0.25	0.5	392
i 2	+	0.25	0.5	261.63

i 2	^+0.5	0.25	0.5	392
i 2	+	0.25	0.5	587.33
i 2	+	0.25	0.5	493.88
i 2	+	0.25	0.5	293.66
i 2	+	0.25	0.5	493.88
i 2	+	0.25	0.5	392
i 2	+	0.25	0.5	783.99

i 2	+	0.25	0.5	440
i 2	+	0.25	0.5	523.25
i 2	+	0.25	0.5	440
i 2	+	0.25	0.5	659.26
i 2	+	0.25	0.5	880
i 2	+	0.25	0.5	440
i 2	+	0.25	0.5	329.63
i 2	+	0.25	0.5	440

i 2 	+	2	0.15	329.63
i 2 	^	.	0.15	392
i 2 	^	.	0.15	493.88

;silence

i 2 	^+4	2	0.15	220
i 2 	^	.	.	261.63
i 2 	^	.	.	329.63

;silence

i 2 	^+4	2	0.15	329.63
i 2 	^	.	.	392
i 2 	^	.	.	493.88

;silence

i 2 	^+4	2	0.15	220
i 2 	^	.	.	261.63
i 2 	^	.	.	329.63

;silence

i 2 	^+4.5	0.125	0.5	440
i 2 	+	0.125	0.5	698,46
i 2 	+	0.125	0.5	587.33
i 2 	+	0.125	0.5	293.66
i 2 	+	1	0.5	783.99 ;ajouter un param pour faire un vibrato. Ou pas, le ping du synthé rend pas mal

i 2 	^+1.5	0.125	0.5	392
i 2 	+	0.125	0.5	659.26
i 2 	+	0.125	0.5	523.25
i 2 	+	0.125	0.5	261.63
i 2 	+	1	0.5	698.46 ;ajouter un param pour faire un vibrato

i 2	^+1.5	0.125	0.5	293.66
i 2 	+	0.125	0.5	493.88
i 2 	+	0.125	0.5	392
i 2 	+	0.125	0.5	196
i 2 	+	1	0.5	523.25 ;ajouter un param pour faire un vibrato

i 2	^+1.5	0.25	0.5	440
i 2	+	0.25	0.5	440
i 2	+	0.16666666666	0.5	440
i 2	+	0.16666666666	0.5	440
i 2	+	0.16666666666	0.5	440
i 2	+	0.125	0.5	440
i 2	+	0.125	0.5	440
i 2	+	0.125	0.5	440
i 2	+	0.125	0.5	440

;arpèges simples et répétitifs
;en ré mineur, à chaque fois basé sur le même accord que celui produit par les nappes
i 2	56	0.125	0.5	698.46 ;tierce mineure à l'octave car l'accord est mineur (différence de 1.5 tons avec l'octave, donc 7.5 tons avec la fondamentale)
i 2	+	0.125	0.5	587.33 ; fondamentale à l'octave (différence de 6 tons avec la fondamentale)
i 2	+	0.125	0.5	440 ; quinte (différence de 3.5 tons avec la fondamentale)
i 2	+	0.125	0.5	293.66 ; fondamentale (le fameux ré ré mineur)
i 2	+	0.125	0.5	698.46 ;1ere répétition
i 2	+	0.125	0.5	587.33
i 2	+	0.125	0.5	440
i 2	+	0.125	0.5	293.66
i 2	+	0.125	0.5	698.46 ;2eme répétition
i 2	+	0.125	0.5	587.33
i 2	+	0.125	0.5	440
i 2	+	0.125	0.5	293.66
i 2	+	0.125	0.5	698.46 ; 3eme répétition
i 2	+	0.125	0.5	587.33
i 2	+	0.125	0.5	440
i 2	+	0.125	0.5	293.66

; en do majeur
i 2	+	0.125	0.5	659.26 ;tierce majeure à l'octave car l'accord est majeur (différence de 2 tons avec l'octave, donc 8 tons avec la fondamentale)
i 2	+	0.125	0.5	523.25 ; fondamentale à l'octave (différence de 6 tons avec la fondamentale)
i 2	+	0.125	0.5	392 ; quinte (différence de 3.5 tons avec la fondamentale)
i 2	+	0.125	0.5	261.63 ; fondamentale (le fameux do de do majeur)
i 2	+	0.125	0.5	659.26 ;1ere répétition
i 2	+	0.125	0.5	523.25
i 2	+	0.125	0.5	392
i 2	+	0.125	0.5	261.63
i 2	+	0.125	0.5	659.26 ;2eme répétition
i 2	+	0.125	0.5	523.25
i 2	+	0.125	0.5	392
i 2	+	0.125	0.5	261.63
i 2	+	0.125	0.5	659.26 ;3eme répétition
i 2	+	0.125	0.5	523.25
i 2	+	0.125	0.5	392
i 2	+	0.125	0.5	261.63

; en sol majeur
i 2	+	0.125	0.5	493.88 ;tierce majeure à l'octave (différence de 2 tons avec l'octave, donc 8 tons avec la fondamentale)
i 2	+	0.125	0.5	392 ; fondamentale à l'octave (différence de 6 tons avec la fondamentale)
i 2	+	0.125	0.5	293.66 ; quinte (différence de 3.5 tons avec la fondamentale)
i 2	+	0.125	0.5	196 ; fondamentale (le fameux sol de sol majeur)
i 2	+	0.125	0.5	493.88 ;1ere répétition
i 2	+	0.125	0.5	392
i 2	+	0.125	0.5	293.66
i 2	+	0.125	0.5	196
i 2	+	0.125	0.5	493.88 ;2eme répétition
i 2	+	0.125	0.5	392
i 2	+	0.125	0.5	293.66
i 2	+	0.125	0.5	196
i 2	+	0.125	0.5	493.88 ;3eme répétition
i 2	+	0.125	0.5	392
i 2	+	0.125	0.5	293.66
i 2	+	0.125	0.5	196

i 2	+	0.125	0.5	440
i 2	+	0.125	0.5	659.26
i 2	+	0.125	0.5	523.25
i 2	+	0.125	0.5	440 
i 2	+	0.125	0.5	783.99
i 2	+	0.125	0.5	523.25
i 2	+	0.125	0.5	440 
i 2	+	0.125	0.5	880
i 2	+	0.125	0.5	523.25
i 2	+	0.125	0.5	440 
i 2	+	0.125	0.5	783.99
i 2	+	0.125	0.5	523.25
i 2	+	0.125	0.5	440 
i 2	+	0.125	0.5	659.26
i 2	+	0.125	0.5	523.25
i 2	+	0.125	0.5	440 

;accord final
i 2	+	2	0.15	587.33
i 2	^	2	0.15	698.46
i 2	^	2	0.15	880
</CsScore>

</CsoundSynthesizer>

