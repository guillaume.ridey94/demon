(ns demon.function)
;cree une ligne de score
(defn addscor [freq, duree, instr, tmp, res]
  (str res "i " instr " " tmp " " duree " " freq "\n"))

;cree une serie de ligne de score
;debut = frequence de depart
;fin = frequence d'arriver
;duree = le temps de chaque note
;pas = l'augnementation entre deux frequence
;instr = l'instrument utilisé
;tmp = le moment du depart de la premiere note
(defn escalier [debut, fin, duree, pas, instr, tmp]
  (loop [i debut,t tmp,res ""]
    (if (>= i fin)
      res
      (recur (+ i pas) (+ t duree) (addscor i duree instr t res)))))

;(printf (escalier 1 10 1 1 5 10))

(def notes_next {"A" 1
                 "B" 0.5
                 "C" 1
                 "D" 1
                 "E" 0.5
                 "F" 1
                 "G" 1})

(def chords_pattern {"4Chords" ['(4.5 2.5 0 3.5) '("min" "Maj" "Maj" "Maj")] ; '(6 4 1 5)
                     "jazz" ['(1 3.5 0) '("min7" "dom7" "Maj7")]}) ; '(2 5 1)

(defn findRoot [gamme toneDiff]
  (loop [note notes_next]
    (if (= gamme (first (first note)))
        (loop [reste toneDiff n note]
          (if (seq n)
              (if (= 0 reste)
                  (hash-map :root (first (first n)))
                  (if (= -0.5 reste)
                      (hash-map :root (first (first n)), :alter "b")
                      (recur (- reste (second (first n))) (rest n))))
              (recur reste notes_next)))
        (recur (rest note)))))


(defn switchQuality [q]
  (case q
    "Maj" "Maj" ; à remplacer par nil plus tard
    "min" "m"
    "min7" "m7"
    "dom7" "dom7"
    "Maj7" "Maj7"))


(defn createChord [gamme nc nq]
  (hash-map :chord (assoc (findRoot gamme nc), :quality (switchQuality nq)), :unit 1, :dur  4))

(defn chords [gamme pattern]
  (loop [acc '()
         nextChords (first (get chords_pattern pattern))
         nextQuality (second (get chords_pattern pattern))]
    (if (seq nextChords)
        (recur (conj acc (createChord gamme (first nextChords) (rest nextQuality)))
               (rest nextChords)
               (rest nextQuality))
        acc)))
