(ns demon.construct
  (:use
   [demon.instrument]
   [demon.score]
   [demon.midiInstr]))


;;spit pour écrire dans un fichier, 1er param le nom du fichier, 2eme ce qu'on veut imprimer


;;on passera la partie ou tout l'edn ?
(defn options_str [param]
  ;for now the options will make csound create a wav file.
  "-odac         ; real time render")


(def exemple {
              :1 (-> {:instr [] :score [] :table [] :cpt 1 :listinstr {} :motif {} :tempo {}}
                    (motif :org1 0 3 :a)
                    (mk-orgue :name :org1 :shape_oscil1 :triangle :shape_oscil2 :sinus
                              :oscil2_octave 2 :amplmax 0.7 :amplmin 0.3 :balance_variation_frequency 2
                              :low_cut_frequency 1800 :resonance 0.127
                              :adsrA 0.3 :adsrD 0.3 :adsrS 1 :adsrR 0.8 :balancer 0.5 )
                    (mk-synth-lead :name :synth1 :shape_oscil :sawtooth
                                   :adsrA2 0.5 :adsrD2 0.1 :adsrS2 0.6 :adsrR2 0.2
                                   :balancer 0.5 :ping_frequency 2 :freqmax 3400 :freqmin 1600)
                    (add-motif :a [(note :RE 2 false),(note :DO 2 true),(note :SOL 1 true),(note :LA 1 false)])
                    (arpege :synth1 32 :RE 3 [[:tiercemin 1] [:fond 1] [:quinte 0] [:fond 0]] 4)
                    (tempo 60))
              } )

;utilise la structure pour generer une string
;qui sera ecrie dans le fichier Csound
(defn affich [struct]
  (str
   "<CsoundSynthesizer>\n\n<CsOptions>\n"
   (options_str nil)
   ;";-odac         ; real time render\n-o orgue.wav -W ; file render"
   "\n</CsOptions>\n\n<CsInstruments>\n"
   "sr = 44100\nksmps = 10\nnchnls = 2\n0dbfs = 1\n\n"
   (write-instr struct)
   "\n</CsInstruments>\n\n<CsScore>\n"
   ";génération des table\n"
   (res-table (:table struct))
   "\n;génération de l'accord du motif :a et de l'arpége\n"
   (write-tempo struct)
   (write-score struct)
   "</CsScore>\n\n</CsoundSynthesizer>"))

;retourne la string compléte du fichier Csound juste avant l'écriture dans le fichier

(defn str_csd [num]
  (affich
   ((keyword num) exemple)))

