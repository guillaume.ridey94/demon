
;; Creation d'une enveloppe
;;A garder pour l'instant
;;cpspch : opcode special non traiter pour l'instant
#_(defn envelope [iatt idec islev irel freq]
    (concat
     (defopcode :-adsr [iatt idec islev irel] [:kenv])
     (defvar "kcps" (str "cpspch(" (name freq) ")"))
     (defopcode :-vco2 [(mult [:kenv 0.8]) :kcps] [:asig])
     (defopcode :-outs [:asig :asig] [])))


;;Enveloppe fonctionnel (renvoie un instrument)
(defn envelope [iatt idec islev irel freq]
  (gen-inst
   {:id 1
    :def (concat
          (defopcode :-adsr [iatt idec islev irel] [:kenv])
          (defvar "kcps" (str "cpspch(" (name freq) ")"))
          (defopcode :-vco2 [(mult [:kenv 0.8]) :kcps] [:asig])
          (defopcode :-outs [:asig :asig] []))}))

(envelope :p5 :p6 :p7 :p8 :p4)

;;Bruit blanc (Noise) fonctionnel (renvoie un instrument)
;; 3 arguement dans le score minimum
;; new opcode "line": trace une ligne entre 2 points (pour signal sonore long et linéaire)
;; new opcode "noise": produit un bruit blanc (arg: amplitude, filter entre -1 et 1)
;; new opcode "Clip": clipping ?
(defn bruit [value]
  (gen-inst
   {:id 2
    :def (concat
          (defopcode :-line [-0.9999 :p3 0.9999] [:kbeta])
          (defopcode :-noise [value :kbeta] [:asig])
          (defopcode :-clip [:asig 2 0.9] [:asig])
          (defopcode :-outs [:asig :asig] []))}))


(bruit 0.3)

;;Creation d'une onde
;;si imode = 10, en dent de scie si imode =0, onde trianglulaire si imode = 4 ou 12 , pulse avec imode= 6, sine wave = question de modulation
(defn wave [amp freq mode]
  (gen-inst
   {:id 3
    :def (concat
          (defopcode :-vco2 [amp freq mode] [:a1])
          (defopcode :-out [:a1] []))}))

(wave 1200 440 10)