(ns demon.midiInstr
  (:use
    [demon.instrument]
    [demon.score]))

;;spit pour écrire dans un fichier, 1er param le nom du fichier, 2eme ce qu'on veut imprimer


(def midi {:ispeed 1
           :iskip 0
           :iloop 1
           :title "SynthTom03.wav"})

(def score_use [[:i-event {:id 1
                           :start 0
                           :dur 1
                           :args [300]
                           :table [1 0 4096 10 1]}]
                [:i-event {:id 2
                           :start 2
                           :dur 1
                           :args [300]
                           :table [2 1 6 256]}]])


(defn midisong [m]
  (str "instr 2\nkSpeed init " (get m :ispeed) "\niSkip init " (get m :iskip) "\n"
       "iLoop init " (get m :iloop) "\na1,asig diskin2 \"" "sample/"(get m :title) "\""
       ", kSpeed, iSkip, iLoop\nout asig,a1\nendin"))



;;;;Rajouter instr1 endin 
;;(defn testMidi []
  ;;(clojure.java.io/file "./test.csd")
  ;;(spit "test.csd" (str_csd nil)))

(defn midisong2 []
  (midisong midi))