(ns demon.instrument
  (:use
   [demon.score]))

;map reliant le mot clef a la string de l'opcode correspondant
(def csound-identifiers
  {:-0dbfs "0dbfs,"
   :-poscil "poscil"
   :-out "out"
   :-outs "outs"
   :-pow "pow"
   :-expseg "expseg"
   :-linseg "linseg"
   :-port "port"
   :-phasor "phasor"
   :-pareq "pareq"
   :-delay1 "delay1"
   :-integ "integ"
   :-vco2 "vco2"
   :-adsr "adsr"
   :-line "line"
   :-noise "noise"
   :-clip "clip"
   :-lfo "lfo"
   :-oscil "oscil"
   :-moogladder "moogladder"
   :-madsr "madsr"
   :-tone "tone"})

;génère la string correspondant a une variable
(defn gen-var [vars]
  (str (:name vars) " = " (:value vars) "\n"))

;génère la string correspondant au argument d'un opcode
(defn gen-arg-opcode [args]
  (loop [tab (rest args), res (str (first args) " ")]
    (if (seq tab)
      (if (or (string? (first tab)) (number? (first tab)))
        (recur (rest tab) (str res "," (first tab) " "))
        (recur (rest tab) (str res "," ((first tab) csound-identifiers) " ")))
      (str res))))


;rend la strucutre correspondant au opération sur les variable
(defn calculvalue [v]
  (loop [tab (:val v),res "("]
    (if (>= 1 (count tab))
      (if (map? (first tab))
        (str res (calculvalue (first tab)) ")")
        (str res (first tab) ")"))
      (if (map? (first tab))
        (recur (rest tab) (str res (calculvalue (first tab)) (:expr v)))
        (recur (rest tab) (str res (first tab) (:expr v)))))))


;rend la structure correspondant au argument des input ,output et des variable
(defn tradinout [value]
  (loop [tab value, res []]
    (if (seq tab)
      (if (map? (first tab))
        (recur (rest tab) (conj res (calculvalue (first tab))))
        (if (keyword? (first tab))
          (recur (rest tab) (conj res (str (name (first tab)))))
          (recur (rest tab) (conj res (str (first tab))))))
      res)))

;génère la string correspondant a un opcode
(defn gen-opcode [opcode]
  (str (gen-arg-opcode (tradinout (:output opcode))) " " ((:id opcode) csound-identifiers) " " (gen-arg-opcode (tradinout (:inputs opcode))) " \n"))



;génère la string correspondant a un instrument
(defn gen-inst [instr]
  (loop [tab (:def instr), x (str "instr " (:id instr) "\n")]
    (if  (seq tab)
      (let [arg (first tab)]
        (case arg
          :var (recur (rest tab) (str x (gen-var (second tab))))
          :opcode (recur  (rest tab) (str x (gen-opcode (second tab))))
          (recur (rest tab) x)))
      (str x " endin\n"))))

;génère la string correspondant en fonction du signe d'une opération
(defn opToString [op]
  (if (= op '+)
    (str "+")
    (if (= op '-)
      (str "-")
      (if (= op '*)
        (str "*")
        (str "/")))))


;rend la strucutre correespondant a une variable
(defn variable [nom, value]
  (if (sequential? value)
    [:var {:name (name nom)
           :value (calculvalue value)}]
    [:var {:name (str (name nom))
           :value (if (keyword? value)
                    (name value)
                    value)}]))




;rend la strcuture qui correspond a une addition
(defn add [args]
      (loop [tab args,res {:expr '+ :val []}]
      (if (seq tab)
        (if (keyword? (first tab))
          (recur (rest tab) (assoc res :val (conj (:val res) (name (first tab)))))
          (recur (rest tab) (assoc res :val (conj (:val res)  (first tab)))))
        res)))

  
;rend la strcuture qui correspond a une soustraction
(defn soust [args]
  (loop [tab args,res {:expr '- :val []}]
      (if (seq tab)
        (if (keyword? (first tab))
          (recur (rest tab) (assoc res :val (conj (:val res) (name (first tab)))))
          (recur (rest tab) (assoc res :val (conj (:val res)  (first tab)))))
        res)))


;rend la strcuture qui correspond a une multiplication
(defn mult [args]
  (loop [tab args,res {:expr '* :val []}]
      (if (seq tab)
        (if (keyword? (first tab))
          (recur (rest tab) (assoc res :val (conj (:val res) (name (first tab)))))
          (recur (rest tab) (assoc res :val (conj (:val res)  (first tab)))))
        res)))

;rend la strcuture qui correspond a une division
(defn div [args]
  (loop [tab args,res {:expr '/ :val []}]
      (if (seq tab)
        (if (keyword? (first tab))
          (recur (rest tab) (assoc res :val (conj (:val res) (name (first tab)))))
          (recur (rest tab) (assoc res :val (conj (:val res)  (first tab)))))
        res)))



;rend la strcuture correspondant a un opcode
(defn opcode [nom input output]
  [:opcode {:id nom
            :inputs  input
            :output  output}])

;rend la structure correspondant a un instrument
(defn gen-instr_num [num ladef]
  (conj (assoc {} :id num) (assoc {} :def ladef)))


;ajoute une table dans la structure des tables
(defn addtable [ table , add]
  (loop [liste add ,res table]
    (if (seq liste)
      (if (contains? res (first liste))
        (recur (rest liste) res)
        (recur (rest liste) (conj res (first liste ))))
      res)))

;modifie le tempo de la strucutre
(defn tempo [struct,temp]
     (conj struct {:tempo temp}))

;ajoute un orgue dans la structure
(defn mk-orgue [struct & {:keys [name,shape_oscil1,shape_oscil2,oscil2_octave,amplmax,amplmin,balance_variation_frequency,low_cut_frequency,resonance,adsrA,adsrD,adsrS,adsrR,balancer]}]
  (conj struct 
        {:instr (conj (:instr struct)
                {:id (:cpt struct)
                 :def (concat
                       (variable :iamp :p4)
                       (variable :ifreq :p5)
                       (opcode :-lfo [1,balance_variation_frequency,1] [:klfo])
                       (opcode :-oscil [:iamp , :ifreq , (first (shape_oscil1 table))] [:ares])
                       (opcode :-oscil [:iamp, (mult [:ifreq,  oscil2_octave]),(first (shape_oscil2 table))] [:asig])
                       (opcode :-moogladder [(add [:ares, (mult [(add [(/(+ amplmax amplmin) 2.0), (mult [(- amplmax (/ (+ amplmax amplmin) 2.0)),:klfo])]),:asig])]),low_cut_frequency,resonance] [:at])
                       (opcode :-madsr [adsrA,adsrD,adsrS,adsrR] [:aenv])
                       (if (< balancer 0)
                         (opcode :-out [(mult [:at,1,:aenv]),(mult [:at,:aenv,(+ 1 balancer)])] [])
                         (opcode :-out [(mult [:at,(- 1 balancer),:aenv]),(mult [:at,:aenv,1])] [])))})
   :table (addtable (:table struct) [shape_oscil1 , shape_oscil2])
   :listinstr (conj {name (:cpt struct )} (:listinstr struct))
   :cpt (+ (:cpt struct) 1)}))



;ajoute un synthé lead dans la structure
(defn mk-synth-lead [struct & {:keys [name,shape_oscil,adsrA2,adsrD2,adsrS2,adsrR2,balancer,ping_frequency
                     freqmax,freqmin]}]
  (conj struct 
        {:instr (conj (:instr struct)
                                {:id (:cpt struct)
                                 :def (concat
                                       (variable :idur :p3)
                                       (variable :iamp :p4)
                                       (variable :ifreq :p5)
                                       (opcode :-oscil [:iamp , :ifreq , (first (shape_oscil table))] [:asig])
                                       (opcode :-lfo [1,ping_frequency,2] [:klfo])
                                       (opcode :-tone [:asig, (add [(/ (+ freqmax freqmin) 2),(mult [:klfo,(- (/ (+ freqmax freqmin) 2) freqmin)])])] [:ares])
                                       (opcode :-madsr [adsrA2,adsrD2,adsrS2,adsrR2] [:aenv])
                                       (if (< balancer 0)
                                         (opcode :-out [(mult [:ares,:aenv,1]),(mult [:ares,:aenv,(+ 1 balancer)])] [])
                                         (opcode :-out [(mult [:ares,:aenv,(- 1 balancer)]),(mult [:ares,:aenv,1])] [])))})
   :listinstr (conj {name (:cpt struct)} (:listinstr struct))
   :table (addtable (:table struct) [shape_oscil])

   :cpt (+ (:cpt struct) 1)}))

;rend la string correspondant a la partie instr de la structure
(defn write-instr [struct]
  (loop [tab (:instr struct),res ""]
    (if (seq tab)
      (recur (rest tab) (str res (gen-inst (first tab))))
      res)))

(defn write-tempo [struct]
  (str (str "t 0 " (:tempo struct)) "\n"))