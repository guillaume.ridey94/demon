(ns demon.score
  (:gen-class))

(declare write-score)
;map reliant les notes avec leur frequence 
(def note-identifiers
  {:DO 32.70
   :DO# 34.65
   :RE 36.71
   :RE# 38.89
   :MI 41.20
   :FA 43.65
   :FA# 46.25
   :SOL 49.00
   :SOL# 51.91
   :LA 55.00
   :LA# 58.27
   :SI 61.74})

;map reliant les nom des tables avec la valeur des tables a mettre dans le scrore
(def table
  {:simple [1,"f 1 0 4096 10 1"]
   :triangle [2,"f 2 0 16384 7  0 4096 1 8192 -1 4097 0"]
   :sawtooth [4,"f 4 0 16384 7  -1 16385 1"]
   :square [6,"f 6 0 16384 7  1 8192 1 0 -1 8192 -1"]
   :sinus [7,"f 7 0 4096 10 1 0 1/3 0 1/5 0 1/7 0 1/9"]})


;rend la frequence d'une note en fonction de l'octave
(defn frequence [note , octav]
  (* (note note-identifiers) (Math/pow 2.0 octav ) ))


;genére la string correspondant a la parti table de la structure
(defn res-table [tab]
  (loop [t tab,res ""]
    (if (seq t)
      (recur (rest t) (str res (second ((first t) table)) "\n"))
      res)))

;génére un la structure d'une note qui sont utilisé dans les accord
(defn note [note , octave , maj]
  {:note note :octave octave :maj maj})


;ajoute un motif d'accord a la structure 
(defn add-motif [struct,name, tab]
  (conj struct {:motif (conj (:motif struct) {name tab})}))


;rend la frequence d'une tierce mineur en fonction de la fondamental
(defn tiercemineur [fond]
  (* 1.0 fond (Math/pow 2 (/ 3.0 12))))

;rend la frequence d'une quinte en fonction de la fondamental
(defn quinte [fond]
  (* 1.0 fond (Math/pow 2 (/ 7.0 12))))

;rend la frequence d'une tierce majeur en fonction de la fondamental
(defn tiercemajeur [fond]
  (* 1.0 fond (Math/pow 2 (/ 4.0 12))))


;ajoute un accord dans la structure
(defn accord [struct, instr, note , octave ,debut, maj]
  (if maj
    (conj struct {:score 
                  (conj (:score struct)
                        [:accord ,
                         [{:instr instr  :param [debut, 4 , 0.222222,(frequence note octave)]},
                        {:instr instr  :param ["^", "." , 0.222222,(tiercemajeur (frequence note octave))]},
                        {:instr instr  :param ["^", "." , 0.222222,(quinte (frequence note octave))]}]])})
    (conj struct {:score 
                  (conj (:score struct)
                        [:accord ,
                         [{:instr instr  :param [debut, 4 , 0.222222,(frequence note octave)]},
                          {:instr instr  :param ["^", "." , 0.222222,(tiercemineur (frequence note octave))]},
                          {:instr instr  :param ["^", "." , 0.222222,(quinte (frequence note octave))]}]])})))




;génére la string correspondant au parametre d'une ligne de score
(defn write-arg [args]
  (loop [tab args, res ""]
    (if (seq tab)
      (recur (rest tab) (str res " " (first tab)))
      (str res "\n"))))


;génére la string correspondant a un accord
(defn write-accord [struct, accord]
  (loop [tab accord , res ""]
    (if (seq tab)
      (if (contains? (:listinstr struct) (:instr (first tab))) 
       (recur (rest tab) (str res "i " ((:instr (first tab)) (:listinstr struct)) (write-arg (:param (first tab)))))
        (throw (Exception. "instrument inconue")))
      res)))

;génére une string correspondante a une repition du motif
(defn aux-motif [struct,instr ,debut , motif]
  (loop [tab (motif (:motif struct)),d debut ,prems true ,r ""]
    (if (seq tab)
      (if prems
        (recur (rest tab) "+" false (str r (write-score (accord (conj struct {:score []}) instr (:note (first tab)) (:octave (first tab)) d (:maj (first tab))))))
        (recur (rest tab) d prems (str r (write-score (accord (conj struct {:score []}) instr (:note (first tab)) (:octave (first tab)) d (:maj (first tab)))))))
      r)))

;génére la string correspondant a un motif
(defn write-aux-motif [struct,instr,debut,nbRep, motif]
  (loop [i nbRep, d debut,prems true, res ""]
    (if (<= i 0)
      res
      (if prems
        (recur (- i 1) "+" false (str res (aux-motif struct instr d motif)))
        (recur (- i 1) d prems (str res (aux-motif struct instr d motif)))))))

;vérifie le motif et génére la string correspondant a un motif
(defn write-motif [struct, motif]
  (if (contains? (:listinstr struct) (:instr motif))
    (if (contains? (:motif struct) (:motif motif))
      (write-aux-motif struct (:instr motif) (:debut motif) (:nbRep motif) (:motif motif))
      (throw (Exception. "motif non present")))
    (throw (Exception. "instrument inconue"))))
  
;génère la string correspondant a la partie score de la structure
(defn write-score [struct]
  (loop [tab (:score struct) , res ""]
    (if (seq tab)
      (case (first (first tab))
        :arpege (recur (rest tab) (str res (write-accord struct (second (first tab)))))
        :accord (recur (rest tab) (str res (write-accord struct (second (first tab)))))
        :motif (recur (rest tab) (str res (write-motif struct (second (first tab))))))
      res)))


;ajoute un motif dans la structure
(defn motif [struct,instr,debut,nbRep, motif]
  (conj struct {:score (conj (:score struct) [:motif , {:instr instr :debut debut :nbRep nbRep :motif motif}])}))

;retourne la frequence d'une ligne d'arpége en fonction de son type et de la frequence de la fondamentale
(defn op-arpege [fond, name]
  (case name
    :quinte (quinte fond)
    :tiercemaj (tiercemajeur fond)
    :tiercemin (tiercemineur fond)
    :fond fond
    ))

;génére une ligne d'un arpége
(defn aux-arpege [instr,debut,note,formule,time,octaveinit,cpt]
  (let [octave (nth (nth formule cpt) 1) ,tipe (nth (nth formule cpt) 0)]
    {:instr instr :param [debut, (/ 1.0 time) , 0.5,(op-arpege  (* (frequence note octaveinit) (Math/pow 2.0 octave)) tipe) ]}))


;ajoute un arpége dans la structure
(defn arpege [struct,instr,debut,note,octave,formule,time]
  (loop [i 0, d debut, res []]
    (if (>= i (* time 4))
      (conj struct {:score (conj (:score struct) [:arpege, res])})
      (recur (+ i 1) "+" (conj res (aux-arpege instr d note formule time octave (mod i 4)))))))


