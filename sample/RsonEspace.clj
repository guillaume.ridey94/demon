(def pre_dec
  {:sr "44100"
   :ksmps "32"
   :nchnls "1"
   :Odbfs "1"})

(def i1
  {:id 1
   :def [:opcode {:id :-prints
                  :inputs ["tone%n"]}
         :opcode {:id :-voc2
                  :inputs ["0.5" "150"]
                  :output [:-aSig]}
         :opcode {:id :-expon
                  :inputs ["10000" "p3" "20"]
                  :output [:-kcf]}
         :opcode {:id :-tone
                  :inputs [:-aSig :-kcf]
                  :output [:-aSig]}
         :opcode {:id :-out
                  :inputs [:-aSig]
                  :output []}]})