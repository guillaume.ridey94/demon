<CsoundSynthesizer>

<CsOptions> 
;-odac
</CsOptions>

<CsInstruments> 
;initialisation des parametre
sr = 44100
kr = 4410
ksmps =10
nchnls=1

instr 1
  ifrq   init cpspch(p5) ;initialise les trois parametre servant a definir la frequence , la longeur et l'amplitude 
  ilen   init        p3
  iamp   init        p4

		;cree la valeur de la frequence qu'on utilise dans le filtre pass-bas
  k2    expseg 3000, 0.08, 9000, ilen, 1
  ksweep =k2-3000
  
  a1    oscil    iamp*0.40, ifrq*0.998-.12, 1 ;cree 4 signal a partir des deux tables
  a2    oscil    iamp*0.40, ifrq*1.002-.12, 2
  a3    oscil    iamp*0.40, ifrq*1.002-.12, 1
  a4    oscil    iamp*0.70, ifrq-.24      , 2
  
  aall= a1+a2+a3+a4 ;association des signaux 
  
  
  a6    butterlp  aall,ksweep ;applique plusieur fois un filtre low-pass
  a8    butterlp  a6,ksweep
  a9    butterhp  a8, 65  
  a10   butterhp  a9, 65  
  a11   butterlp  a10,1000
  
  asig  linen    a11, p6, ilen, p7 ;applique un motif de chute avec une attaque
  
  
  out asig
endin

</CsInstruments>

<CsScore>
f1 0   2048 10 1 1 1 1 .7 .5 .3 .1         ;pulse
f2 0   1024 10  1                    

i1      0      .4      99500  5.00        .02       .01
i1      +      .       .      5.02        .         .  
i1      +      .26667  .      5.04        .         .  
i1      +      .26667  .      5.05        .         .  
i1      +      .26667  .      5.07        .         .  
</CsScore>

</CsoundSynthesizer>
