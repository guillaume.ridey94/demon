# demon

A Clojure library designed to ... well, that part is up to you.

## Usage

se programe génére un fichier "test.csd". Pour lancer le programme il faut lancer le run du lein avec pour argument le numero de la structure a genérer qui se trouve dans le fichier "construct.clj". par exemple la commande "lein run 1" lacera la generation de la premieres structure dans le fichier "test.csd".

## License

Copyright © 2020 FIXME

This program and the accompanying materials are made available under the
terms of the Eclipse Public License 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

This Source Code may also be made available under the following Secondary
Licenses when the conditions for such availability set forth in the Eclipse
Public License, v. 2.0 are satisfied: GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or (at your
option) any later version, with the GNU Classpath Exception which is available
at https://www.gnu.org/software/classpath/license.html.
