(defproject demon "0.1.0-SNAPSHOT"
  :description "Declarative (Electronic) Music in Clojure (with CSound)"
  :url "https://gitlab.com/fredokun/demon"
  :license {:name "The MIT License"
            :url "http://opensource.org/licenses/MIT"}
  :dependencies [[org.clojure/clojure "1.10.0"]]
  :repl-options {:init-ns demon.core}
  :main demon.core)
